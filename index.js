let http = require("http");

let port = 4000;

http.createServer((request, response)=>{

    // The HTTP method of the incoming request can be accessed via the method property of "request" parameter.
    // The method "GET" means that we will be retrieving or reading the information.
    if(request.url == "/items" && request.method == "GET"){
        // Requests the "/items" path and "GETS" information
        response.writeHead(200, {'Content-Type': 'text/plain'});
        // ends the response process
        response.end('Data retrieved from the database!');
    };

    // The method "POST" means that we will be adding or creating information
    if(request.url = "/addItems" && request.method == "POST"){
        // request the "/addItems" path and "SENDS" information
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Succesfully added Items!');
    };

}).listen(port);

console.log(`Server is running at localhost: ${port}`);