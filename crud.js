let http = require("http");

// mock database

let directory = [
    {
        "name": "Brandon",
        "email": "brandon@mail.com"
    },
    {
        "name": "Jobert",
        "email": "Jobert@mail.com"
    }
];

// GET METHOD
http.createServer(function(request, response){
    // Route for returning all items upon receiving a GET request
    if(request.url == "/users" && request.method == "GET"){
        response.writeHead(200, {'Content-Type': 'application/json'});
        // JSON.stringify() - method that convert the string?JS to JSON
        response.write(JSON.stringify(directory));
        response.end();
    }

// POST METHOD
    if(request.url == "/addUser" && request.method == "POST"){

        let requestBody = "";
        request.on('data', function(data){
            requestBody += data;
        });

        request.on('end',function(){
            console.log(typeof requestBody);

            // Converts the string requestBody to JS
            requestBody = JSON.parse(requestBody);

            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }

            directory.push(newUser);
            console.log(directory);

            response.writeHead(200, {'Content-Type': 'application/json'});
            response.write(JSON.stringify(newUser));
            response.end();
        });
    }

}).listen(3000);

console.log(`Server running at localhost: 3000`);